﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class UploadExcelController : Controller
    {
        // GET: UploadExcel
        public ActionResult Index()
        {
            return View();
        }

        static string ExcelFilePath = "";

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path = Path.Combine(Server.MapPath("~/Images"),
                                               Path.GetFileName(file.FileName));
                    if (!Directory.Exists(Server.MapPath("~/Images")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Images"));
                    }
                    file.SaveAs(path);
                    ExcelFilePath = path;
                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            return View();
        }

        [HttpPost]
        public void ParseExcelFile()
        {
            FileInfo fileInfo = new FileInfo(ExcelFilePath);

            ExcelPackage package = new ExcelPackage(fileInfo);
            ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();

            // get number of rows and columns in the sheet
            int rows = worksheet.Dimension.Rows; // 20
            int columns = worksheet.Dimension.Columns; // 7

            // loop through the worksheet rows and columns
            for (int i = 1; i <= rows; i++)
            {
                for (int j = 1; j <= columns; j++)
                {

                    string content = worksheet.Cells[i, j].Value.ToString();

                    int id;
                    bool flag = int.TryParse(content, out id);

                    if (flag)
                    {
                        List<Model> lsit = new List<Model>()
                {
                    new Model(){ID= 1},
                    new Model(){ID= 1},
                    new Model(){ID= 1},
                    new Model(){ID= 1},
                    new Model(){ID= 1},
                    new Model(){ID= 1},
                    new Model(){ID= 1}
                   };

                        CreateNewExcel(lsit, id);

                    }

                }
            }
        }

        List<string[]> headerRow = new List<string[]>()
{
  new string[] { "ID", "First Name", "Last Name", "DOB" }
};


        void CreateNewExcel(List<Model> vs, int id)
        {

            //add header to file
            string headerRange = "A1:" +
                char.ConvertFromUtf32(headerRow[0].Length + 64) +
                 "1";



            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add("Worksheet1");

                var excelWorksheet = excel.Workbook.Worksheets["Worksheet1"];


                //must send a model to "LoadFromCollection" and in Model 
                //Must have properties 
                excelWorksheet.Cells[2, 1].LoadFromCollection(vs, true);


                //save  file to a Pad
                FileInfo excelFile = new FileInfo(Server.MapPath("~/Images") +
                    "/" + id + ".xls");
                excel.SaveAs(excelFile);

            }



        }

        class Model
        {
            public int ID { get; set; }
        }


    }
}